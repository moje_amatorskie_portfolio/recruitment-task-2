<?php

require "../Start/process_route.php";

if (@$_SESSION['login']) {
    route('/logout', 'LoginController', 'logout');
    route('/list', 'ItemController', 'index');
    route('/add', 'ItemController', 'add');
    route('/edit', 'ItemController', 'edit');
    route('/delete', 'ItemController', 'delete');
    route('/validate', 'ItemController', 'validate');
    route('/seed', 'ItemController', 'seed');
} else {
    route('/login', 'LoginController', 'login');
    route('/login_validate', 'LoginController', 'validate');
}
