<?php

session_start();
setcookie(session_name(), session_id(), time()+600000, '/');

function view($view, $params)
{
    global $app_url;
    extract($params);
    require("../Views/".$view.".php");
}
