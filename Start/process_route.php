<?php

$uri = $_SERVER['REQUEST_URI'];
$is_route_matched = false;

function route($route, $controller, $action)
{
    global $uri;
    global $app_url;
    global $is_route_matched;
    if ($is_route_matched == false && $uri == $app_url."/index.php".$route) {
        $is_route_matched = true;
        require("../Controllers/".$controller.".php");
        $action();
    }
}
