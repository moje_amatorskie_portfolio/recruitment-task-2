<?php

$config = parse_ini_file('../.env');
$app_url = $config["APP_URL"];
$db = $config["DB"];
$db_user = $config["DB_USER"];
$db_password = $config["DB_PASSWORD"];
$app_title = $config["APP_NAME"];
