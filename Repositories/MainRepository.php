<?php

require("BaseRepository.php");

function getItems()
{
    $query = "select  items.id, title, description, categories.category, status from items inner join categories on items.category_id = categories.id order by title";
    
    return sqlQuery($query, 'select');
}
function getItem($id)
{
    $query = "select  items.id, title, description, categories.category, status from items inner join categories on items.category_id = categories.id where items.id=$id";
    
    return sqlQuery($query, 'select');
}
function doesUserExist($email)
{
    $query = "select email from users where email='$email'";
    
    return sqlQuery($query, 'select');
}
function doesPasswordMatch($password, $email)
{
    $query = "select password from users where email='$email'";
    $result = sqlQuery($query, 'select');
    // die(password_hash('password', PASSWORD_DEFAULT));
    
    return password_verify($password, $result[0]['password']);
}
function getCategories()
{
    $query = "select id, category from categories";
    
    return sqlQuery($query, 'select');
}
function seedDatabase()
{
    $query ="INSERT INTO `items` VALUES (0,'Glass','Description of glass',3,1),(0,'Pan','Description of pan',3,1),(0,'Bowl','Description of bowl',3,1),(0,'Hose','Description of hose',2,1),(0,'Flowerpot','A flowerpot is a container in which flowers and other plants are cultivated.',1,1),(0,'Garden soil','Description of soil',2,1),(0,'Table','Description of table',1,1),(0,'Chair','Description of chair',1,1),(0,'Sofa','Description of sofa',2,0),(0,'Cloth','Description of cloth',3,1),(0,'Carpet','Description of carpet',1,1),(0,'Bed','A bed is a piece of furniture which is used as a place to sleep, relax. Most modern beds consist of a soft, cushioned mattress.',1,1),(0,'Cooker','Electric cooker – an electric powered cooking device for heating and cooking of food.',3,0),(0,'Fridge','A refrigerator or fridge is a home appliance consisting of a thermally insulated compartment and a heat pump.',3,1)";
    
    return sqlQuery($query, 'seed');
}
function insertOrUpdate($fields, $id = null)
{
    extract($fields);
    if ($id) {
        $query = "update items set title='$title', description='$description', category_id='$selectedCategoryId', status='$selectedStatus' where id='$id'";
    } else {
        $query = "insert into items values(0, '$title', '$description', '$selectedCategoryId', '$selectedStatus')";
    }

    return sqlQuery($query, 'insertOrUpdate');
}
function deleteItem($id)
{
    $query = "delete from items where id=$id";
    
    return sqlQuery($query, 'delete');
}
function sqlQuery($query, $type)
{
    $connection = open_database_connection();
    //to prevent sql injection one can also use prepare() and execute()
    $result = $connection->query($query);
    if ($type == 'select' && $result != false) {
        $rows = [];
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $rows[] = $row;
        }
    } else {
        if ($result == true) {
            $insertSuccess = true;
        } else {
            $insertSuccess = false;
        }
    }
    close_database_connection($connection);

    return $type == 'select' ? @$rows : @$insertSuccess;
}
