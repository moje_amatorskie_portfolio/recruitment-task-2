<?php

function open_database_connection()
{
    $connection = new PDO("mysql:host=localhost;dbname=aurora", 'aurora', 'aurora');

    return $connection;
}

function close_database_connection(&$connection)
{
    $connection = null;
}
