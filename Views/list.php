<?php ob_start() ?>
    <div class="content">

        <div class="notice">
            <?php echo @$notice ? $notice."<i class='fa fa-thumbs-up' style='margin-left: 5px;'></i>" : '' ?>
        </div>

        <?php foreach ($items as $item): ?>
            
        <div class="item">
            
            <div class="field title">
                <?php echo $item['title'] ?>
            </div>
            <div class="field description">
                <?php echo $item['description'] ?>
            </div>
            <div class="field category">
                <?php echo $item['category'] ?>
            </div>
            <div class="field status">
                <?php echo $item['status'] ? "available" : "not available" ?>
            </div>
            <div class="field button-forms" >
                <form class="button-form" action="<?php echo $app_url ?>/index.php/edit" method="POST">
                    <input hidden type="text" name="id" value="<?php echo $item['id'] ?>">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-edit"></i>&nbsp;&nbsp;Edit
                    </button>
                </form>
                <form class="button-form" action="<?php echo $app_url ?>/index.php/delete" method="POST">
                    <input hidden type="text" name="id" value="<?php echo $item['id'] ?>">
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>&nbsp;&nbsp;Del
                    </button>
                </form>
            </div>
        </div>
        
        <?php endforeach ?>
        
    </div>
    
<?php $content = ob_get_clean() ?>

<?php require("parts/layout.php") ?>