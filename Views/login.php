<?php ob_start() ?>

    <div class="content" >
        <div class="notice">
            <?php echo @$notice ? $notice."<i class='fa fa-thumbs-up' style='margin-left: 5px;'></i>" : '' ?>
        </div>
        <form style="margin-top: 150px" action="<?php echo $app_url ?>/index.php/login_validate" method="POST">
            <div class="error text-center">
                <?php require("parts/errors.php") ?>
            </div>
          <div class="form-outline mb-4" style="box-shadow: 8px 8px 4px 0 rgba(0, 0, 0, 0.08)">
              <input name="email" type="email" class="form-control" placeholder="Email address" value="user@user.pl"/>
          </div>
          <div class="form-outline mb-4" style="box-shadow: 8px 8px 4px 0 rgba(0, 0, 0, 0.08)">
              <input name="password" type="password" class="form-control" placeholder="Password" value="password"/>
          </div>
          <div class="row mb-4">
              <div class="col d-flex justify-content-center">
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" checked/>
                      <label class="form-check-label" style="width:100%;"> Remember </label>
                  </div>
              </div>
          </div>
          <button type="submit" class="btn btn-secondary btn-block mb-4 w-100" >Log in</button>
          <div class="text-center" >
              <p hidden>Not a member? <a  style="text-decoration: none;" href="<?php echo $app_url ?>/index.php/register">Register</a></p>
          </div>
        </form>
    </div>            
    
<?php $content = ob_get_clean() ?>

<?php require("parts/layout.php") ?>
