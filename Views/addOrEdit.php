
<?php ob_start() ?>
    <div class="content">
        <form class="item add-or-edit" action=" <?php echo $app_url.'/index.php/validate' ?>" method="POST">
            <div class="error text-center">
                <?php require("parts/errors.php") ?>
            </div>
            <div class="field title ">
                <textarea style="height: 100%;background-color: #e3dede;" name="title"  placeholder="title" ><?php echo @$title ? $title : '' ?></textarea>
            </div>
            <div class="field description">
                <textarea style="height: 100%;;background-color: #e3dede;" name="description"  placeholder="description" ><?php echo @$description ? $description : '' ?></textarea>
            </div>
            <div class="field category ">
                <select style="height: 100%;;background-color: #e3dede;" name="categoryId">
                    <?php foreach ($categories as $category): ?>
                        <option style="background:#e5f1ef" value="<?php echo $category['id'] ?>" <?php echo @$selectedCategory == $category['category'] ? 'selected' : ''?>>
                            <?php echo $category['category'] ?>
                        </option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="field status" >
                <select style="height: 100%;;background-color: #e3dede;" name='status'>
                    <option value="1" <?php echo @$selectedStatus ? 'selected' : ''?> >
                        Available
                    </option>
                    <option value="0" <?php echo @$selectedStatus ? '' : 'selected'?>>
                        Not available
                    </option>
                </select>
            </div>
            <input hidden name='type' value="<?php echo @$type ?>">
            <input hidden name='id' value="<?php echo @$_POST['id'] ?> "/>
            <div class="field submit">
                <button class="btn btn-success" type='submit'>Save</button>                
            </div>
        </form>
    </div>
    
<?php $content = ob_get_clean() ?>

<?php require("parts/layout.php") ?>