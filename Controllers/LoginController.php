<?php

require("../Repositories/MainRepository.php");

function login()
{
    return view('login', ['param' => 'param']);
}

function validate()
{
    $errors = [];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $doesUserExist = doesUserExist($email);
    if ($doesUserExist == false) {
        $errors[] = "This email does not exist in database.";
    } else {
        $doesPasswordMatch = doesPasswordMatch($password, $email);
        if ($doesPasswordMatch == false) {
            $errors[] = "Entered password is not correct.";
        } else {
            $_SESSION['login'] = $email;
        }
    }
    if ($errors) {
        return view('login', compact('errors'));
    }
    $notice="You've logged in successfully";
    $items = getItems();

    return view('list', compact('items', 'notice'));
}

function logout()
{
    unset($_SESSION['login']);
    return view('login', ['notice' => 'You have been logged out']);
}
