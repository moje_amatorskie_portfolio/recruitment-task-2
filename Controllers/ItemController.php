<?php

require("../Repositories/MainRepository.php");

function index($notice = null)
{
    $items = getItems();
    
    return view('list', ['items' => $items, 'notice' => $notice]);
}

function add()
{
    addOrEdit('add');
}

function edit()
{
    addOrEdit('edit');
}

function delete()
{
    deleteItem($_POST['id']);
    $notice = "Item has been deleted successfully";
    index($notice);
}

function addOrEdit($type)
{
    $categories = getCategories();
    if ($type == 'add') {
        return view('addOrEdit', compact('categories', 'type'));
    } else {
        $item = getItem($_POST['id']);
        extract($item[0]);
        $selectedStatus = $status;
        $selectedCategory = $category;
        
        return view('addOrEdit', compact('categories', 'type', 'title', 'description', 'selectedStatus', 'selectedCategory'));
    }
}

function validate()
{
    $categories = getCategories();
    $title = $_POST['title'];
    $description = $_POST['description'];
    $selectedCategoryId = $_POST['categoryId'];
    $selectedStatus = $_POST['status'];
    $type = $_POST['type'];
    $errors = [];
    $id = @$_POST['id'];
    if (strlen($title) > 30) {
        $errors[] = "Title can't be longer than 30 characters.";
    }
    if (strlen($description) > 255) {
        $errors[] = "Description can't be longer than 255 characters.";
    }
    if (strlen($title) < 2) {
        $errors[] = "Title should be longer than 1 character :)";
    }
    if (strlen($description) < 2) {
        $errors[] = "Description should be longer than 1 character :)";
    }
    if ($errors) {
        return view('addOrEdit', compact('categories', 'title', 'description', 'selectedCategoryId', 'selectedStatus', 'errors', 'type'));
    } else {
        $notice = '';
        if ($type == 'add') {
            $result = insertOrUpdate(compact('title', 'description', 'selectedCategoryId', 'selectedStatus'));
            if ($result == true) {
                $notice = 'Item has been saved successfully';
            } else {
                $notice = 'There was some error while saving item';
            }
        } else {
            $result = insertOrUpdate(compact('title', 'description', 'selectedCategoryId', 'selectedStatus', 'id'));
            if ($result == true) {
                $notice = 'Item has been saved successfully';
            } else {
                $notice = 'There was some error while saving item';
            }
        }
        $items = getItems();

        return view('list', compact('items', 'notice'));
    }
}

function seed()
{
    seedDatabase();
    $items = getItems();
    
    return view('list', ['items' => $items, 'notice' => 'Database has been seeded successfully']);
}
